package com.reobotenet.dao;

import java.io.Serializable;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;

import com.reobotenet.exception.NegocioException;
import com.reobotenet.model.Produto;
import com.reobotenet.util.jpa.Transactional;

public class ProdutoDAO implements Serializable{

		private static final long serialVersionUID = 1L;
		
		@Inject
		private EntityManager manager;
		
		@Transactional
		public Produto salvar(Produto produto){
			
			try {
				produto = this.manager.merge(produto);
				return produto;
				
			} catch (OptimisticLockException e) {
				
				throw new NegocioException("Erro de concorrência. Esse usuário já foi alterado anteriormente." + e);
				
			}
			
		}
		
		public Produto buscarPeloCodigo(Long codigo){
			
			Produto produto = this.manager.find(Produto.class, codigo);
			
			if(produto == null){
				throw new NegocioException("Usuário não encontrado.");
			}
			return produto;
			
			 
		}

}
